provider "aws" {
  region = var.region
  access_key = var.access_key
  secret_key = var.secret_key
}

# Backend for State file
terraform {
  backend "s3" {
    bucket         = "terraformjenkinsawsforlife"
    key            = "terraform.tfstate"
    region         = "eu-west-1"
    encrypt        = true
  }
}

# resource "aws_key_pair" "ubuntu" {
#   key_name   = "jenkins-slave-key"
#   public_key = ""
# }

module "vm_creation" {
  source = "./modules"
  instance_type = "t2.nano"
}
