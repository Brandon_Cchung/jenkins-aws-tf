variable "access_key" {
  type = "string"
}
variable "secret_key" {
  type = "string"
}
variable "region" {
  type = "string"
  default = "eu-west-1"
}

# Standard Variables
variable "bucket_name" {
  type = "string"
  default = "terraformjenkinsawsforlife"
}

variable "vpc_name" {
  type = "string"
  default = "flask-vpc"
}
